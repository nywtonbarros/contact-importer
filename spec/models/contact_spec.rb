require 'rails_helper'

RSpec.describe Contact, type: :model do
  describe 'validations' do
    subject { build :contact }

    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to allow_value('Nywton Barros').for(:name) }
    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to allow_value('email@addresse.foo').for(:email) }
    it { is_expected.to validate_presence_of(:address) }
    it { is_expected.to validate_presence_of(:phone) }
    it { is_expected.to allow_value('(+57) 123 456 78 90').for(:phone) }
    it { is_expected.to validate_presence_of(:dob) }

    it { is_expected.to belong_to(:contacts_csv_file) }
    it { is_expected.to have_one(:user) }
  end
end
