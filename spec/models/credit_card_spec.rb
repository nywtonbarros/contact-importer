require 'rails_helper'

RSpec.describe CreditCard, type: :model do
  describe 'validations' do
    subject { build :credit_card }

    it { is_expected.to validate_presence_of(:last_four) }
    it { is_expected.to validate_presence_of(:card_number) }
    it { is_expected.to validate_presence_of(:franchise) }

    it { is_expected.to belong_to(:contact) }
  end
end
