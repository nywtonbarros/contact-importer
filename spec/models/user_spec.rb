require 'rails_helper'

RSpec.describe User, type: :model do
  describe 'validations' do
    subject { build :user }

    it { is_expected.to validate_presence_of(:email) }
    it { is_expected.to have_many(:csv_files) }
    it { is_expected.to have_many(:contacts) }
  end
end
