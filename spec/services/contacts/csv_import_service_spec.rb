require 'rails_helper'

RSpec.describe 'Contacts::CsvImportService', type: :service do

  describe '.execute' do
    let(:file) { create :contacts_csv_file }

    context 'when it have valid any contacts' do

      it 'should create contacts and change' do
        expect { Contacts::CsvImportService.execute(file) }.to change { Contact.count }.by(150)
      end

      it 'Should change file status to finished' do
        expect { Contacts::CsvImportService.execute(file) }.to change { file.reload.status_before_type_cast }
          .from(Contacts::CsvFile.statuses[:on_hold]).to(Contacts::CsvFile.statuses[:finished])
      end

      context 'when it have not valid contacts' do
        let(:file) { create :contacts_csv_file, :empty_file }

        it 'should create contacts and change' do
          expect { Contacts::CsvImportService.execute(file) }.to change { Contact.count }.by 0
        end
      end
    end
  end
end
