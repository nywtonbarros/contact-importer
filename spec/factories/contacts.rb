FactoryBot.define do
  factory :contact do
    name { Faker::Name.unique.name }
    email { Faker::Internet }
    dob { Faker::Date.between(from: 28.years.ago, to: 20.years.ago) }
    phone { Faker::PhoneNumber.cell_phone }
    address { Faker::Address.street_address }

    trait :with_credit_card do
      credit_cards { [build(:credit_card)] }
    end
  end
end
