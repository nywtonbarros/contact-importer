FactoryBot.define do
  factory :contacts_csv_file, class: 'Contacts::CsvFile' do
    status { Contacts::CsvFile.statuses[:on_hold] }
    user

    after(:build) do |file|
      path = File.join(Rails.root, '/spec/fixtures/contacts_mock.csv')
      file.data = Rack::Test::UploadedFile.new(path, 'text/csv')
      file.name = 'contacts_mock.csv'
    end

    trait :empty_file do
      after(:build) do |file|
        path = File.open("#{Rails.root}/spec/fixtures/empty_contacts_mock.csv")
        file.data = Rack::Test::UploadedFile.new(path, 'text/csv')
        file.name = 'empty_contacts_mock.csv'
      end
    end
  end
end
