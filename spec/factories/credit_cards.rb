FactoryBot.define do
  factory :credit_card do
    last_four { '' }
    franchise { '' }
    encrypted_card_number { '' }
    card_number { '4111111111111111' }
    contact { build(:contact) }
  end
end
