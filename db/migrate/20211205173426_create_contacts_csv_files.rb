class CreateContactsCsvFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts_csv_files do |t|
      t.belongs_to :user, null: false, foreign_key: true
      t.integer :status, :integer, null: false, default: 0
      t.string :name, null: false

      t.timestamps
    end
  end
end
