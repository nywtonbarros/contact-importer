class AddContactsCsvFileIdToContacts < ActiveRecord::Migration[6.1]
  def change
    add_reference :contacts, :contacts_csv_file, null: false, foreign_key: true
  end
end
