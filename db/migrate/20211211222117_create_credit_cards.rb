class CreateCreditCards < ActiveRecord::Migration[6.1]
  def change
    create_table :credit_cards do |t|
      t.belongs_to :contact, null: false, foreign_key: true
      t.string :last_four, null: false
      t.string :franchise, null: false
      t.string :encrypted_card_number, null: false

      t.timestamps
    end
  end
end
