class CreateContacts < ActiveRecord::Migration[6.1]
  def change
    create_table :contacts do |t|
      t.string :name
      t.string :email
      t.date :dob
      t.string :phone
      t.string :address

      t.timestamps
    end
  end
end
