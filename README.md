# Contact CSV Imports

### Requirements

* Ruby 3.0.2
* Rails 6.1.4
* Database Postgres
* (PostgreSQL) 14.1


### Live Testing
[https://koombea-contact-import.herokuapp.com](https://koombea-contact-import.herokuapp.com/)
![home_image](https://bitbucket.org/nywtonbarros/contact-importer/raw/a23f909dbaadeb3364234393758e8294db196fc0/app/assets/images/home_screen.png)
```
User: teste@koombea.com
password: iam@koombea123
```

A example CSV file format can be found at:
[specs/fixtures/contacts_mock.csv](https://bitbucket.org/nywtonbarros/contact-importer/src/main/spec/fixtures/contacts_mock.csv)
```
specs/fixtures/contacts_mock.csv
```

Configure Application:
```
$ bundle install
$ rake db:setup
```

Running Application:
```
# run the rails server
$ rails s -b 0.0.0.0

# For frontend assets
$ bin/webpack-dev-server
```

Testing:
```
$ bundle exec rspec
```

