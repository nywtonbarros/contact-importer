Rails.application.routes.draw do
  devise_for :users

  namespace :contacts do
    resources :csv_files
  end

  resources :contacts

  root to: 'contacts/csv_files#index'
end
