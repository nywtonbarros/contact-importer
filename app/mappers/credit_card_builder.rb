class CreditCardBuilder
  attr_accessor :credit_card

  def initialize
    @credit_card = CreditCard.new
  end

  def self.build_from(csv_line)
    builder = new
    builder.credit_card.card_number = csv_line['card_number']
    return builder.credit_card.errors.add(:card_number, :blank) if csv_line['card_number'].blank?

    builder.last_four
    builder.franchise_from_card_number
    builder.encrypts_card_number
    builder.credit_card
  end

  def franchise_from_card_number
    CreditCard::BRANDS_REGEX.find do |brand|
      @credit_card.franchise = brand[0] if brand[1].match?(@credit_card.card_number)
    end
  end

  def last_four
    @credit_card.last_four = @credit_card.card_number[-4..]
  end

  def encrypts_card_number
    @credit_card.encrypted_card_number = Digest::SHA1.hexdigest(@credit_card.card_number)
  end
end
