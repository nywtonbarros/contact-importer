class ContactBuilder
  class << self
    def build_from(csv_line, file_id)
      Contact.new(
        name: csv_line['name'],
        dob: csv_line['date_of_birth'],
        address: csv_line['address'],
        phone: csv_line['phone'],
        email: csv_line['email'],
        contacts_csv_file_id: file_id,
        credit_cards: [CreditCardBuilder.build_from(csv_line)]
      )
    end
  end
end
