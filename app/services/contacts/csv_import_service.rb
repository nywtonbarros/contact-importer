require 'csv'
module Contacts
  class CsvImportService
    class << self
      def execute(file)
        contact_attributes(file).each_with_index do |contact_line, index|
          contact = ContactBuilder.build_from(contact_line, file.id)
          contact.save ? on_success(file, index, contact) : on_error(file, index, contact)
        end
      end

      private

      def contact_attributes(file)
        CSV.parse(file.data.download, headers: true, skip_blanks: true, skip_lines: /^(?:,\s*)+$/)
      end

      def on_success(file, index, contact)
        file.finished!
        Rails.logger
             .info("Successfully processed line: #{index}, file: #{file.name}. Errors: #{contact.errors.full_messages}")
      end

      def on_error(file, index, contact)
        Rails.logger
             .error("Failed to process line: #{index}, file: #{file.name}, Errors: #{contact.errors.full_messages}")
      end
    end
  end
end
