module Contacts
  class CsvFileJob < ApplicationJob
    queue_as :default

    def perform(file_id)
      file = Contacts::CsvFile.find(file_id)
      file.processing!
      Contacts::CsvImportService.execute file
      file.processing? ? nothing_imported(file) : on_finish(file)
    end

    def nothing_imported(file)
      file.failed!
      Rails.logger
           .error("Failed to process file: #{file.name}. Nothing imported.")
    end

    def on_finish(file)
      Rails.logger
           .info("Successfully processed file: #{file.id}.")
    end
  end
end
