module Contacts
  class CsvFilesController < ApplicationController
    def index
      @contacts_csv_files = Contacts::CsvFile.all
    end

    def show
      @contacts_csv_file = Contacts::CsvFile.find(params[:id])
    end

    def new
      @contacts_csv_file = Contacts::CsvFile.new
    end

    def create
      @csv_file = Contacts::CsvFile.new(name: contacts_csv_file_params['data']&.original_filename,
                                        data: contacts_csv_file_params['data'],
                                        user_id: current_user.id)

      if @csv_file.save
        Contacts::CsvFileJob.perform_later @csv_file.id
        redirect_to contacts_csv_files_url, notice: 'The file was successfully uploaded and will be processed soon.'
      else
        render :new
      end
    end

    private

    def contacts_csv_file_params
      params.require(:contacts_csv_file).permit(:data)
    end
  end
end
