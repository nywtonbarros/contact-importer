class User < ApplicationRecord
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable

  has_many :csv_files, class_name: 'Contacts::CsvFile'
  has_many :contacts, through: :csv_files
end
