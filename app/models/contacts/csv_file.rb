# frozen_string_literal: true

module Contacts
  class CsvFile < ApplicationRecord
    self.table_name = 'contacts_csv_files'

    has_one_attached :data
    belongs_to :user

    has_many :contacts, foreign_key: :contacts_csv_file_id

    enum status: {
      on_hold: 0,
      processing: 1,
      failed: 2,
      finished: 3
    }

    ACCEPTABLE_HEADERS = %w[name date_of_birth phone email address].freeze
    ACCEPTABLE_TYPES = ['text/csv'].freeze

    MAX_FILE_SIZE = 10.megabytes

    validates :name, :data, presence: true

    private

    def file_format
      errors.add(:base, 'invalid type') unless ACCEPTABLE_TYPES.include?(data.content_type)
    end

    def valid_file_size
      errors.add(:base, 'too large') if data.attached? && data.blob.byte_size > MAX_FILE_SIZE
    end

    def valid_headers
      return unless attachment_changes['data']&.attachable.present?

      begin
        headers = CSV.open(attachment_changes['data'].attachable, 'r', &:first)

        errors.add(:base, 'invalid_headers') unless headers.sort == ACCEPTABLE_HEADERS.sort
      rescue CSV::MalformedCSVError
        errors.add(:base, 'malformed file structure')
      end
    end
  end
end
