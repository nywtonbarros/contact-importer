class Contact < ApplicationRecord
  VALID_NAME_REGEX = /\A[a-zA-Z -]+\z/
  VALID_DOB_REGEX = %r{\b\d{4}([-/ ])\d{1,2}\1\d{1,2}\b}
  VALID_PHONE_REGEX = /\(?\+?[0-9]{2}\)\s?[0-9]{3}(\ |-)[0-9]{3}(\s|-)[0-9]{2}(\s|-)[0-9]{2}/

  belongs_to :contacts_csv_file, class_name: 'Contacts::CsvFile', foreign_key: :contacts_csv_file_id
  has_one :user, through: :contacts_csv_file
  has_many :credit_cards

  accepts_nested_attributes_for :credit_cards

  validates :address, presence: true
  validates :name, presence: true, format: { with: VALID_NAME_REGEX }
  validates :email, presence: true, uniqueness: true, format: { with: URI::MailTo::EMAIL_REGEXP }
  validates :dob, presence: true, format: { with: VALID_DOB_REGEX }
  validates :phone, presence: true, format: { with: VALID_PHONE_REGEX }
end
