class CreditCard < ApplicationRecord
  BRANDS_REGEX = {
    visa: /^4[0-9]{12}(?:[0-9]{3})?$/,
    master_card: /^(?:5[1-5][0-9]{2}|222[1-9]|22[3-9][0-9]|2[3-6][0-9]{2}|27[01][0-9]|2720)[0-9]{12}$/,
    american_express: /^3[47][0-9]{13}$/,
    diners_club: /^3(?:0[0-5]|[68][0-9])[0-9]{11}$/,
    discover: /^6(?:011|5[0-9]{2})[0-9]{12}$/,
    jcb: /^(?:2131|1800|35\d{3})\d{11}$/
  }.freeze

  belongs_to :contact

  attr_accessor :card_number

  validates :card_number, presence: true, length: { minimum: 10, maximum: 19 }
  validates :last_four, presence: true
  validates :franchise, presence: true
end
